﻿using UnityEngine;
using System.Collections;
using System.Collections;
using UnityEditor;
public class CreateAtlas : ScriptableWizard {
	public Texture2D[] Textures;
	public string AtlasName ="Atlas_Texture";


	[MenuItem("GameObject/Create Other/Create Atlas")]
	static void CreateWizard()
	{
		ScriptableWizard.DisplayWizard ("Create Atlas", typeof(CreateAtlas));
	}

	void OnWizardCreate(){


	
	}

	//Fucnction to configure texture for atlasing
	public void ConfigureForAtlas(string TexturePath)
	{
		TextureImporter TexImport = AssetImporter.GetAtPath(TexturePath) as TextureImporter;
		TextureImporterSettings tiSettings = new TextureImporterSettings();
		
		TexImport.textureType = TextureImporterType.Advanced;
		
		TexImport.ReadTextureSettings(tiSettings);
		
		tiSettings.mipmapEnabled = false;
		tiSettings.readable = true;
		tiSettings.maxTextureSize = 4096;
		tiSettings.textureFormat = TextureImporterFormat.ARGB32;
		tiSettings.filterMode = FilterMode.Point;
		tiSettings.wrapMode = TextureWrapMode.Clamp;
		tiSettings.npotScale = TextureImporterNPOTScale.None;
		
		TexImport.SetTextureSettings(tiSettings);
		
		//Re-import/update Texture
		AssetDatabase.ImportAsset(TexturePath, ImportAssetOptions.ForceUpdate);
		AssetDatabase.Refresh();
	}
	void GenerateAtlas(){
		foreach (Texture2D t in Textures) {
			ConfigureForAtlas(AssetDatabase.GetAssetPath(t));
		
		}
	}
}
